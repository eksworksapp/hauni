﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


    [ExecuteInEditMode]
public class FastSetProp : MonoBehaviour {

    public Transform from;
    public Transform to;

    public bool Globals = true;

    public bool Set = false;
	void Start () {
	}
	
	void Update () {
        if (Set)
        {
            Debug.Log("Run");
            if (to == null)
                to = transform;
            if (Globals)
            {
                to.position = from.position;
                to.rotation = from.rotation;

                var p = to.parent;

                to.SetParent(null);
                to.localScale = from.lossyScale;
                to.SetParent(p);
            }
            else
            {

                to.localPosition = from.localPosition;
                to.localRotation = from.localRotation;

                to.localScale = from.localScale;
                ///to.SetParent(p);
            }
            Set = false;
        }
	}
}
