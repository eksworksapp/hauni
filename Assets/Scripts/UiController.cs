﻿using UnityEngine;
using UnityEngine.Events;
using System;
using System.Collections;
using UnityEngine.UI;

public static class Globals
{
    public static int CurrentId = -1;
    public static bool Forsed = false;
    public static bool CanSelect = false;
    public static bool FullPreview = false;
}

public class UiController : MonoBehaviour
{
    public enum Mode { View, VR, Full};

    public RectTransform PhotoTargeter;
    public Renderer Fader;
    public float maxFadeAlpha = 0.8f;

    public Animator InfoButtonAnimator;
    public Animator SelectButtonAnimator;
    public Animator AboutPanel;

    public InfoPanelController InfoPanel;

    public Transform MainContainer;
    public Transform HandlePoint;
    public Image[] buttonsBG;

    public GameObject ButtonRoot;

    public delegate void OnTargetEvent(bool OnFinded, GameObject from);
    public static OnTargetEvent onTargetEvent;

    public delegate void OnClikedOnAgregate(int id);
    public delegate void OnCustomEvent(int id, Transform from);


    public static OnClikedOnAgregate onClickedOnAgregate;
    public static OnClikedOnAgregate onZoom;
    public static OnClikedOnAgregate onExplode;
    public static OnCustomEvent forcedDetach;

    public static UnityAction<bool> onEmailOpen;


    private Mode _mode = Mode.VR;

    public Mode mode {
        get
        {
            return _mode;
        }
        set
        {
            _mode = value;

            //SelectButtonAnimator.SetBool("IsShow", mode != Mode.VR || Globals.CanSelect);
            InfoButtonAnimator.SetBool("IsShow", mode == Mode.View);
            //PhotoTargeter.gameObject.SetActive(mode == Mode.VR && !finded);

        }
    }

    public bool handPreview;
    bool finded = false;

    private Material _fadeMaterial;

    private Transform _handledOldParent;
    private Color def;
    void Start()
    {
        onTargetEvent += OnTarget;
        onClickedOnAgregate += OnButtonClick;
        _fadeMaterial = Fader.material;

        _handledOldParent = MainContainer.parent;


        def = buttonsBG[0].color;

        var c = def;
        c.a = 0;
        foreach (var b in buttonsBG)
            b.color = c;

        /*
        // магия 
        onEmailOpen += x =>
        {
            ButtonRoot.SetActive(!x);
        };
         * */
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    private bool AboutOpened = false;
    private bool ioldPrevState = false;
    private bool buttosState = false;

    public void AboutPanelState(bool show = false)
    {
        AboutPanel.SetBool("IsShow", show);
        if (show ) {
            ioldPrevState = PhotoTargeter.gameObject.activeSelf;
            PhotoTargeter.gameObject.SetActive(false);
            AboutOpened = true;
            SelectButtonAnimator.SetBool("IsShow", false);
        } else
        {
            AboutOpened = false;
            SelectButtonAnimator.SetBool("IsShow", buttosState);
            PhotoTargeter.gameObject.SetActive(ioldPrevState);
            
        }

        onEmailOpen.Invoke(AboutOpened);
    }
    

    void OnTarget(bool finded, GameObject from)
    {
        //Debug.Log("OnTarget:" + from.name + "status:" + finded);

        if (!finded && this.finded)
        {

            if (mode == Mode.View && from != _handledOldParent.gameObject)
            {
                Debug.Log("[FORCED]" + from.name);
                forcedDetach(Globals.CurrentId, _handledOldParent);
            }
            Handling();

        } else
        {
            if (from != _handledOldParent.gameObject)
            {
                if (mode == Mode.View)
                {
                    forcedDetach(Globals.CurrentId, from.transform );
                }
                UnHandling();
            }
            else
            {

                UnHandling(false);
               
            }
        }
      

        if (finded && !this.finded && !AboutOpened)
        {
            PhotoTargeter.gameObject.SetActive(false);
            SelectButtonAnimator.SetBool("IsShow", true);
            buttosState = true;
        }

        this.finded = finded;
    }

    private Vector3 _oldPosition;
    private Quaternion _oldRot;
    void Handling()
    {
        if (handPreview)
            return;

        handPreview = true;
        //Сбрасываем
        _oldPosition = MainContainer.localPosition;
        _oldRot = MainContainer.localRotation;

        MainContainer.SetParent(HandlePoint);
        MainContainer.gameObject.SetActive(true);
       
        MainContainer.localRotation = Quaternion.identity;
        MainContainer.localPosition = Vector3.zero;

        /*
        Globals.FullPreview = true;
        if (null != onExplode)
            onExplode(0);*/

    }

    void UnHandling(bool disable = true)
    {
        if (!handPreview)
            return;

        MainContainer.SetParent(_handledOldParent);
        MainContainer.localPosition = _oldPosition;
        MainContainer.localRotation = _oldRot;

        if (disable)
            MainContainer.gameObject.SetActive(false);
        /*
        Globals.FullPreview = false;
        if (null != onExplode)
            onExplode(0);*/

        handPreview = false;
    }
    

    public void EmailToOut()
    {
        if (AboutPanel.GetBool("IsShow"))
        {
            AboutPanelState();
        }
        else
        {
            OnButtonClick(-1);
            AboutPanelState(true);
        }
    }
    
    public void OnExploreClick()
    {
        AboutPanelState();

        if(mode == Mode.View)
        {
            Globals.CanSelect = false;
            onZoom(0);
            OnButtonClick(-1);
        }
        else
        {
            Globals.CanSelect = !Globals.CanSelect;
            onZoom(0);
        }
    }

    IEnumerator FadeInumerator(bool show = false)
    {
        Color seta = def; seta.a = 0;
        
        Color color = _fadeMaterial.GetColor("_Color");
        Fader.gameObject.SetActive(true);

        foreach (var b in buttonsBG)
            b.color = !show ? seta: def ;
        

        while (true)
        {
            if (show)
            {
                color.a += 0.05f;
                if (color.a > maxFadeAlpha)
                {
                    break;
                }
            }
            else
            {
                color.a -= 0.05f;
                if (color.a <= 0)
                {

                    Fader.gameObject.SetActive(false);
                    break;
                }
            }

            _fadeMaterial.SetColor("_Color", color);
            yield return new WaitForSeconds(0.01f);
        }
    }
   
    bool self = false;
    public void OnButtonClick(int id)
    {
        //AboutPanelState();
        if (self)
        {
            self = false;
            return;
        }

        if (!handPreview && !finded)
            return;

        if (onClickedOnAgregate != null)
        {
            self = true;
            onClickedOnAgregate(id);
        }

        mode = id > -1 ? Mode.View : Mode.VR;

        StartCoroutine(FadeInumerator(mode == Mode.View));

        if (Globals.CurrentId != id)
        {
            Globals.CurrentId = id;
            InfoPanel.HidePanel();
            //InfoPanel.ShowPanel();
        }

        //PhotoTargeter.gameObject.SetActive(mode == Mode.VR && !finded);
        //InfoButtonAnimator.SetBool("IsShow", id > -1);

    }
}
