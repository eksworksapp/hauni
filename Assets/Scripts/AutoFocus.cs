﻿using UnityEngine;
using System.Collections;
using Vuforia;
public class AutoFocus : MonoBehaviour {

    [SerializeField]
	private CameraDevice.FocusMode mFocusMode = CameraDevice.FocusMode.FOCUS_MODE_NORMAL;
    
    void Start () {

        Invoke("InvokeFocus", 3f);
	}

    public void InvokeFocus()
    {
        CameraDevice.Instance.SetFocusMode(mFocusMode);
        Invoke("InvokeFocus", 3f);
    }
}
