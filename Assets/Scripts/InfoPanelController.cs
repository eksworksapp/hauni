﻿using UnityEngine;
using System.Collections;

public class InfoPanelController : MonoBehaviour {

    private Animator _animator;
          
    void Awake()
    {
        _animator = GetComponent<Animator>();
    }

    public void ToogleButton()
    {
        if (_animator.GetBool("IsShow"))
        {
            HidePanel();
        } else
        {
            ShowPanel();
        }
    }

    GameObject lastShowed = null;    
    public void ShowPanel()
    {
        if (Globals.CurrentId == -1 || Globals.CurrentId >= transform.childCount)
            return;

        if (lastShowed != null)
            lastShowed.SetActive(false);

        lastShowed = transform.GetChild(Globals.CurrentId).gameObject;
        lastShowed.SetActive(true);
        _animator.SetBool("IsShow", true);
    }

    public void HidePanel()
    {
        _animator.SetBool("IsShow", false);
    }
}
