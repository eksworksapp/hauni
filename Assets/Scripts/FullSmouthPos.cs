﻿using UnityEngine;

public class FullSmouthPos : MonoBehaviour
{
    public bool _enableMotions = false;
    public bool ScaleAlways = true;
    public GameObject Apparat;
    public GameObject TargetApparat;

    public float smoothTime = 0.9F;
    private float smoothTimeR = 0.5F;
    //publpublic float smoothTime = 0.9F;ic Transform target;
    private Vector3 velocityPos = Vector3.zero; //  позиция

    private Vector3 velocityScal = Vector3.zero; // размер

    private Vector3 velocityRotXYZ = Vector3.zero; // варащ
    private float velocityRotW = 0; // вращение

    private Vector3 RoXYZ;
    private float RoW;

    public float ScaleSize = 1f;

    private Transform _target;
    private Transform _apparat;

    private RotateIt _rotatteIt;
    private TBPinchToScale _pinch;

    private bool _autoStop = false;

    void Awake()
    {
        _target = TargetApparat.transform;
        _apparat = Apparat.transform;
        _rotatteIt = GetComponent<RotateIt>();
        _pinch = GetComponent<TBPinchToScale>();

        EnableMotions = _enableMotions;
    }

    public bool EnableMotions
    {
        set
        {
            _enableMotions = value;
            _autoStop = false;
            _rotatteIt.enabled = _enableMotions;
            _pinch.enabled = _enableMotions;
        }
        get
        {
            return _enableMotions;
        }
    }

    private Vector3 targetRotXYZ = Vector3.zero;
    private Quaternion _rotation = Quaternion.identity;
    void Update()
    {

        if (ScaleAlways || _enableMotions)
        {
            //Vector3 targetScale = _target.localScale * ScaleSize; //  плавно
            _apparat.localScale = Vector3.SmoothDamp(_apparat.localScale, _target.localScale * ScaleSize, ref velocityScal, smoothTime);
        }

        if (!_enableMotions)
        {
            return;
        }

        //Vector3 targetPosition = TargetApparat.transform.position; //  плавно
        _apparat.localPosition = Vector3.SmoothDamp(_apparat.localPosition, _target.localPosition, ref velocityPos, smoothTime);


        //  плавно
        targetRotXYZ.x = _target.localRotation.x;
        targetRotXYZ.y = _target.localRotation.y;
        targetRotXYZ.z = _target.localRotation.z;

        RoXYZ = Vector3.SmoothDamp(RoXYZ, targetRotXYZ, ref velocityRotXYZ, smoothTimeR);

        //Vector3 targetRotBB = new Vector3(TargetApparat.transform.rotation.w, 0, 0);
        RoW = Mathf.SmoothDamp(RoW, _target.localRotation.w, ref velocityRotW, smoothTimeR);
        _rotation.Set(RoXYZ.x, RoXYZ.y, RoXYZ.z, RoW);

        _apparat.localRotation = _rotation;

        if (_autoStop)
        {
            if(Vector3.Distance(_apparat.localPosition,_target.localPosition) < 0.01f &&
               Mathf.Abs(_target.localScale.sqrMagnitude - _apparat.localScale.sqrMagnitude) < 0.01f &&
               Vector3.Distance(RoXYZ, targetRotXYZ) < 0.01f &&
               Mathf.Abs(RoW - _target.localRotation.w) < 0.01f)
            {
                _enableMotions = false;
                _autoStop = false;
            }

        }
    }
    
    public void AutoStop()
    {
        _rotatteIt.enabled = false;
        _pinch.enabled = false;
        _autoStop = true;
        _enableMotions = true;
    }
}
