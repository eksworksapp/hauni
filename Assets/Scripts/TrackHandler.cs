﻿using UnityEngine;
using Vuforia;

public class TrackHandler : MonoBehaviour, ITrackableEventHandler
{
    public Transform[] Targets;

    private TrackableBehaviour mTrackableBehaviour;

    void Start()
    {

        if (Targets.Length == 0)
        {
            Targets = new Transform[transform.childCount];
            for (int i = 0; i < transform.childCount; i++)
                Targets[i] = transform.GetChild(i);
        }

        mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        if (mTrackableBehaviour)
        {
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            OnTracking(true);
            if (UiController.onTargetEvent != null)
                UiController.onTargetEvent(true, this.gameObject);
        }

        if (Input.GetKeyDown(KeyCode.RightShift))
        {
            OnTracking(false);
            if (UiController.onTargetEvent != null)
                UiController.onTargetEvent(false, this.gameObject);
        }
    }

    public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
    {
        var state = newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED;
        //Debug.Log("BBBBB");
        OnTracking(state);
        if (UiController.onTargetEvent != null)
            UiController.onTargetEvent(state, this.gameObject);
    }

    private void OnTracking(bool found = false)
    {
        for (int i = 0; i < Targets.Length; i++)
        {
            Targets[i].gameObject.SetActive(found);
        }
       // Debug.Log("aaaaaaaaaa");
    }

}
