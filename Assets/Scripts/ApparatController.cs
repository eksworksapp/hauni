﻿using UnityEngine;
using System.Collections;
using System;

public class ApparatController : MonoBehaviour
{

    [SerializeField]
    private Transform Target;
    [SerializeField]
    private int ID = 0;

    public bool MainObject = false;
    public bool CanBeClicked = true;
    public float ScaleOnCam = 2f;

    public bool Detached = false;
    public float ZoomCoeff = 1.2f;
    //public Transform ExplodeTarget;

    private FullSmouthPos _fullPos;
    private Transform _child;
    private Transform _transform;

    private Transform _oldParent;
    [SerializeField]
    private Vector3 _startPos;
    [SerializeField]
    private Quaternion _startRot;
    [SerializeField]
    private Vector3 _startScale;

    private bool _wasStarted = false;
    [SerializeField]
    private bool _reposed = false;

    void Awake()
    {
        UiController.onClickedOnAgregate += OnClicked;
        UiController.onZoom += Zoom;
        UiController.forcedDetach += NeedAttachToTarget;

        UiController.onEmailOpen += SetShowingStatus;
        Debug.Log("[INIT AWAKE]" + gameObject.name + " from: " + transform.parent.parent.name);
    }

    void Start()
    {

        if (_wasStarted)
            return;

        _wasStarted = true;
        _fullPos = GetComponent<FullSmouthPos>();
        _transform = transform;
        _child = _fullPos.Apparat.transform;

        _startPos = _transform.localPosition;
        _startRot = _transform.localRotation;
        _oldParent = _transform.parent;
        _startScale = _transform.localScale;

        //Debug.Log("[INIT START]" + gameObject.name + " from: " + _transform.parent.name);

    }

    public void SetShowingStatus(bool emailOpen)
    {
        if (_child == null)
            return;

        var renders = _child.GetComponentsInChildren<MeshRenderer>();
        foreach (var item in renders)
        {
            item.enabled = !emailOpen;
        }
    }

    /*
    public void UpdateInfo()
    {
        _startPos = _transform.localPosition;
    }
     */

    public void NeedDetachFromTarget()
    {
        Debug.Log("[DETACHER]" + name);
        Detached = false;

        _transform.SetParent(_oldParent);
        _child.SetParent(_oldParent);

        _transform.localPosition = _child.localPosition = _startPos;
        _transform.localRotation = _child.localRotation = _startRot;
        _transform.localScale = _child.localScale = _startScale;


        _fullPos.EnableMotions = false;
        _child.gameObject.SetActive(false);
        //_fullPos.AutoStop();
    }

    public void NeedAttachToTarget(int id, Transform parent)
    {
        //if (parent != _transform.parent)
        //    return;
        print("Need Attach Target");
        if (id != ID)
        {
            _child.gameObject.SetActive(false);
            return;
        }


        // Debug.Log("[ATTACHER] from " + parent + " to " + _transform.parent.parent.name);

        if (_transform.parent.parent != parent)
            return;

        _child.gameObject.SetActive(true);

        var _target = Target.GetComponentInChildren<ApparatController> ();
       // _target.UpdateInfo();
        var _tpos = _target != null ? _target.transform.localPosition : Vector3.zero;
        var _trot = _target != null ? _target.transform.localRotation : Quaternion.identity;
        var _tscl = _target != null ? _target.transform.localScale : Vector3.one * ScaleOnCam;

        if (_target != null)
            _target.NeedDetachFromTarget();

        Detached = true;
        _transform.SetParent(Target);
        _child.SetParent(Target);

        _transform.localPosition = _child.localPosition = _tpos;
        _transform.localRotation = _child.localRotation = _trot;
        _transform.localScale = _child.localScale = _tscl;


        _fullPos.EnableMotions = true;

    }

    void OnMouseDown()
    {
        if (!CanBeClicked)
            return;

        if (UiController.onClickedOnAgregate != null)
            UiController.onClickedOnAgregate(ID);
    }

    void OnClicked(int id)
    {

        Start();

        if (!_oldParent.gameObject.activeSelf && id == ID)
            return;


        if (id != -1 && id != ID)
            _child.gameObject.SetActive(false);
        else
            _child.gameObject.SetActive(true);

        if (id == ID)
        {
            if (!Detached)
                GoToCamera();
        }
        else
        {

            if (Detached)
                GoToStart();
        }
    }
    /*
    public void OnRepos(int id)
    {
        if (Detached)
            _reposed = false;


        if (ExplodeTarget == null || !MainObject)
            return;

        // Start();
        //Debug.Log(gameObject.name);
        if (Globals.FullPreview)
        {
            _transform.localPosition = ExplodeTarget.localPosition;
            _transform.localRotation = ExplodeTarget.localRotation;
            _reposed = true;
        }
        else
        {
            _transform.localPosition = _startPos;
            _transform.localRotation = _startRot;
            _reposed = false;
        }
        _fullPos.AutoStop();
    }
    */
    public void Zoom(int i)
    {
        if (!_wasStarted)
            return;

        if (Globals.CanSelect)
        {
            _transform.localScale = _startScale * ZoomCoeff;
            CanBeClicked = true;
        }
        else
        {
            _transform.localScale = _startScale;
            CanBeClicked = false;
        }
    }

    public void GoToCamera()
    {
        if (Detached)
            return;

        Detached = true;
        _transform.SetParent(Target);
        _child.SetParent(Target);
        _transform.localPosition = Vector3.zero;
        _transform.localRotation = Quaternion.identity;
        _transform.localScale = Vector3.one * ScaleOnCam;
        _fullPos.EnableMotions = true;
    }

    public void GoToStart()
    {
        if (!Detached)
            return;

        Detached = false;
        _transform.SetParent(_oldParent);
        _child.SetParent(_oldParent);

       // if (!_reposed)
       // {
            _transform.localPosition = _startPos;
            _transform.localRotation = _startRot;
       // }
       // else
       // {
       //     _transform.localPosition = ExplodeTarget.localPosition;
       //     _transform.localRotation = ExplodeTarget.localRotation;
       // }
        _transform.localScale = _startScale;

        _fullPos.AutoStop();
    }

    public void OnEnable()
    {
        if (!_wasStarted)
            return;

        _transform.localPosition = _startPos;
        _transform.localRotation = _startRot;
        _transform.localScale = _startScale;

        _child.localPosition = _startPos;
        _child.localRotation = _startRot;
        _child.localScale = _startScale;
    }
}
