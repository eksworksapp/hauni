﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
public class LoadNextScene : MonoBehaviour {

    public string Name;
    public float Time = 8f;

	IEnumerator Start () {

        yield return new WaitForSeconds(Time);
        SceneManager.LoadScene(Name, LoadSceneMode.Single);    
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
