﻿using UnityEngine;
using System.Collections;

public class ButtonSelectedScript : MonoBehaviour {

    [SerializeField]
    private int ID = 0;
    [SerializeField]
    private Color NormalColor;
    [SerializeField]
    private Color Selected;

    int oldState = 0;
    UnityEngine.UI.Image _image;

	void Start () {
        _image = GetComponent<UnityEngine.UI.Image>();
        oldState = Globals.CurrentId;
	}
	
	// Update is called once per frame
	void Update () {

        if(oldState != Globals.CurrentId)
        {
            _image.color = Globals.CurrentId == ID ? Selected : NormalColor;
            oldState = Globals.CurrentId;
        }
	}
}
