﻿using UnityEngine;
using System.Collections;

public class AboutPanelScaler : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Canvas root = FindObjectOfType<Canvas>();
        GetComponent<RectTransform>().sizeDelta = new Vector2(0, root.GetComponent<RectTransform>().sizeDelta.y);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
