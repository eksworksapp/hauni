/*==============================================================================
Copyright (c) 2010-2014 Qualcomm Connected Experiences, Inc.
All Rights Reserved.
Confidential and Proprietary - Protected under copyright and other laws.
==============================================================================*/

using UnityEngine;

namespace Vuforia
{
    /// <summary>
    /// A custom handler that implements the ITrackableEventHandler interface.
    /// </summary>
    public class DefaultTrackableEventHandler : MonoBehaviour,
                                                ITrackableEventHandler
    {
        #region PRIVATE_MEMBER_VARIABLES
 
        private TrackableBehaviour mTrackableBehaviour;
		public Vector3 PosCam;
        public Quaternion QuaRott; public int MarkOffOnn = 0;
		public GameObject CamMe;
        #endregion // PRIVATE_MEMBER_VARIABLES



        #region UNTIY_MONOBEHAVIOUR_METHODS
		void Awake() {
			CamMe = GameObject.Find("ARCamera"); ////
			PosCam = CamMe.transform.position;////
			QuaRott = CamMe.transform.rotation;/////
		}
        void Start()
        {

			//CamMe = GameObject.Find("Camera"); ////
			//PosCam = Camera.main.transform.position;////
			//QuaRott = Camera.main.transform.rotation;/////


            mTrackableBehaviour = GetComponent<TrackableBehaviour>();
            if (mTrackableBehaviour)
            {
                mTrackableBehaviour.RegisterTrackableEventHandler(this);
            }
        }

        #endregion // UNTIY_MONOBEHAVIOUR_METHODS



        #region PUBLIC_METHODS

        /// <summary>
        /// Implementation of the ITrackableEventHandler function called when the
        /// tracking state changes.
        /// </summary>
        public void OnTrackableStateChanged(
                                        TrackableBehaviour.Status previousStatus,
                                        TrackableBehaviour.Status newStatus)
        {
            if (newStatus == TrackableBehaviour.Status.DETECTED ||
                newStatus == TrackableBehaviour.Status.TRACKED ||
                newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
            {
                OnTrackingFound();
            }
            else
            {
                OnTrackingLost();
            }
        }

        #endregion // PUBLIC_METHODS



        #region PRIVATE_METHODS
		private int AdderSh;
		void Update()
		{
			AdderSh = AdderSh + 1;
			if (AdderSh == 4)
			{
				
				Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
				Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);
				// Disable rendering:
				foreach (Renderer component in rendererComponents)
				{
					component.enabled = false;
				}
			}

			if (MarkOffOnn == 0) { /////
			//	CamMe.transform.position = PosCam;/////
			//	CamMe.transform.rotation = QuaRott;/////
			}  //
		}
        private void OnTrackingFound()
        {
            Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
            Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);

            // Enable rendering:
            foreach (Renderer component in rendererComponents)
            {
                component.enabled = true;
            }

            // Enable colliders:
            foreach (Collider component in colliderComponents)
            {
                component.enabled = true;
            }
			MarkOffOnn = 1; //////

            Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " found");
        }


        private void OnTrackingLost()
        {
            Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
            Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);

            // Disable rendering:
            foreach (Renderer component in rendererComponents)
            {
				component.enabled = false;  //component.enabled = false;
            }

            // Disable colliders:
            foreach (Collider component in colliderComponents)
            {
				component.enabled = false;    // component.enabled = false;
            }
			MarkOffOnn = 0; ///
			CamMe.transform.position = PosCam;/////
			CamMe.transform.rotation = QuaRott;/////

            Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " lost");
        }

        #endregion // PRIVATE_METHODS
    }
}
